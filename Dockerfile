FROM python:3.6

ARG ACCOUNT_NAME=pythonapp

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY app.py .

# upgrade pip and install requirements
RUN pip install --upgrade pip \
 && pip install --no-cache-dir -r requirements.txt

# create user/group that container will run as
RUN groupadd -r ${ACCOUNT_NAME} \
 && useradd -r -m -s /bin/bash -g ${ACCOUNT_NAME} ${ACCOUNT_NAME}

EXPOSE 5000

USER ${ACCOUNT_NAME}

CMD [ "python", "./app.py" ]
